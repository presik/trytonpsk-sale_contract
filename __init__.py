# This file is part of the sale_weight module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import account
from . import sale
from . import contract
from . import configuration
# import opportunity


def register():
    Pool.register(
        account.Invoice,
        configuration.Configuration,
        configuration.ConfigurationSequence,
        contract.TypeContract,
        contract.SaleContract,
        contract.SaleContractLine,
        contract.ContractProductLine,
        contract.ContractProductTax,
        contract.SaleContractByMonthStart,
        contract.CreateInvoicesFromContractStart,
        contract.SaleContractFromPartyStart,
        sale.Sale,
        # opportunity.CrmOpportunityLine,
        module='sale_contract', type_='model')
    Pool.register(
        contract.SaleContractByMonth,
        contract.CreateInvoicesFromContract,
        contract.SaleContractFromParty,
        module='sale_contract', type_='wizard')
    Pool.register(
        contract.SaleContractReport,
        contract.SaleContractByMonthReport,
        module='sale_contract', type_='report')
