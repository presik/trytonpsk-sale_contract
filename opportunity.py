from trytond.model import fields
from trytond.pool import PoolMeta


class CrmOpportunityLine(metaclass=PoolMeta):
    __name__ = "crm.opportunity.line"
    unit_price = fields.Numeric('Unit Price', digits=(16, 2))
