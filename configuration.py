from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Id, Eval


def default_func(field_name):
    @classmethod
    def default(cls, **pattern):
        return getattr(
            cls.multivalue_model(field_name),
            'default_%s' % field_name, lambda: None)()
    return default


class Configuration(metaclass=PoolMeta):
    __name__ = 'sale.configuration'
    sale_contract_sequence = fields.MultiValue(fields.Many2One('ir.sequence',
        'Contract Sequence', required=False,
        domain=[
            ('sequence_type', '=',
                Id('sale_contract', 'sequence_type_sale_contract')),
            ('company', 'in',
                [Eval('context', {}).get('company', -1), None]),
            ]
        ))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'sale_contract_sequence':
            return pool.get('sale.configuration.sequence')
        return super(Configuration, cls).multivalue_model(field)

    default_sale_sequence = default_func('sale_contract_sequence')


class ConfigurationSequence(metaclass=PoolMeta):
    __name__ = 'sale.configuration.sequence'
    sale_contract_sequence = fields.Many2One(
        'ir.sequence', "Sale Contract Sequence", required=True,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('sequence_type', '=', Id('sale_contract', 'sequence_type_sale_contract')),
            ],
        depends=['company'])
